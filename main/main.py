import copy
import json
import redis
import requests
import sys
import xml.etree.ElementTree as ET
from multiprocessing import Pool


def run(city, checkin, checkout):
    pool = Pool()
    snaptravel_hotels_results = pool.apply_async(request_snaptravel_hotels, [city_string_input, checkin_string_input, checkout_string_input])
    retail_hotel_prices_results = pool.apply_async(request_retail_hotel_prices, [city_string_input, checkin_string_input, checkout_string_input])
    snaptravel_hotels = snaptravel_hotels_results.get(timeout=10_000)
    retail_hotel_prices = retail_hotel_prices_results.get(timeout=10_000)
    combined_hotels = combine(snaptravel_hotels, retail_hotel_prices)
    persist(combined_hotels)


def request_snaptravel_hotels(city, checkin, checkout):
    """Returns a list of hotels from snaptravel hotels request given city, checkin, and checkout"""

    url = "https://experimentation.getsnaptravel.com/interview/hotels"
    body = {
        "city": city,
        "checkin": checkin,
        "checkout": checkout,
        "provider": "snaptravel"
    }
    response = requests.post(url, json=body)
    return response.json()["hotels"]


def request_retail_hotel_prices(city, checkin, checkout):
    """Returns a dictionary of IDs to retail price from retail hotels request given city, checkin, and checkout"""

    url = "https://experimentation.getsnaptravel.com/interview/legacy_hotels"
    body = """\
<?xml version="1.0" encoding="UTF-8"?>
<root>
  <checkin>{checkin}</checkin>
  <checkout>{checkout}</checkout>
  <city>{city}</city>
  <provider>snaptravel</provider>
</root>""".format(city=city, checkin=checkin, checkout=checkout)
    headers = {'Content-Type': 'application/xml'}
    response = requests.post(url, data=body, headers=headers)
    root = ET.fromstring(response.content)
    results = {int(element.find("id").text): float(element.find("price").text) for element in root}
    return results


def combine(snaptravel_hotels, retail_hotel_prices):
    """Returns a list of combined hotels consisting of snaptravel hotels in the given list that have a corresponding
    retail price in the given dictionary.

    A combined hotel is identical to the snaptravel hotel except price is replaced with a dictionary of the snaptravel
    price and retail price."""

    result = []
    for hotel in snaptravel_hotels:
        if hotel["id"] in retail_hotel_prices:
            combined_hotel = copy.deepcopy(hotel)
            combined_hotel["price"] = {
                "snaptravel": hotel["price"],
                "retail": retail_hotel_prices[hotel["id"]]
            }
            result.append(combined_hotel)
    return result


def persist(combined_hotels):
    """Store combined_hotels in redis as json keyed by ID"""

    print(combined_hotels)
    try:
        r = redis.Redis()
        r.mset({hotel["id"]: json.dumps(hotel) for hotel in combined_hotels})
    except redis.exceptions.RedisError as err:
        print("redis failure: ", err)


if __name__ == "__main__":
    city_string_input = sys.argv[1]
    checkin_string_input = sys.argv[2]
    checkout_string_input = sys.argv[3]
    run(city_string_input, checkin_string_input, checkout_string_input)
